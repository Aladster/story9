from django.shortcuts import render,redirect
from django.contrib import messages
from django.http import JsonResponse
import requests
import json
from .forms import SignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

def home(request):
    return render(request, 'main/home.html')

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    #print(ret.content)
    data = json.loads(ret.content)
    #data = {"hehe":"hehe"}
    return JsonResponse(data, safe=False)


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('main:home')
    else:
        form = SignUpForm()
    return render(request, 'main/signup.html', {'form': form})


def Login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('main:home')
        else:
            messages.info(request, 'Username or password is incorrect')
    context = {}
    return render(request, 'main/login.html', context)

def Logout(request):
    logout(request)
    return redirect('main:home')
